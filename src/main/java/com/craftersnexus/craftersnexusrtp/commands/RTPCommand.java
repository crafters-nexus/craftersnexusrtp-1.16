package com.craftersnexus.craftersnexusrtp.commands;

import com.craftersnexus.craftersnexusrtp.ConfigHandler;
import com.craftersnexus.craftersnexusrtp.CraftersNexusRTP;
import com.craftersnexus.craftersnexusrtp.RandomTeleporter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import java.util.Arrays;

public class RTPCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String cmdmessage, String[] args) {
        if(command.getName().equalsIgnoreCase("rtp")) {
            // Plain RTP command.
            if (args.length == 0) {
                if (!(commandSender instanceof Player)) {
                    commandSender.sendMessage("You must be a player to use this command!");
                    return true;
                }
                Player player = (Player) commandSender;
                if (!player.hasPermission("craftersnexus.rtp")) {
                    player.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
                    return true;
                }
                World world = player.getWorld();
                String worldName = world.getName().toLowerCase();
                // Is the world enabled for RTP?
                if (ConfigHandler.worldNotEnabled(worldName)) {
                    player.sendMessage(ChatColor.RED + "RTP in this world is not enabled!");
                    return true;
                }
                // All checks passed, do random teleport!
                return doRandomTeleport(player, world);
            }
            // RTP command with world as argument
            if(args.length == 1 && !isAddOrRemoveCommand(args[0])) {
                if (!(commandSender instanceof Player)) {
                    commandSender.sendMessage("You must be a player to use this command!");
                    return true;
                }
                Player player = (Player) commandSender;
                // Get the world name from the args
                String worldName = args[0];
                World world = Bukkit.getWorld(worldName);
                if (world == null) {
                    player.sendMessage(ChatColor.RED + "The world: " + worldName + " does not exist.");
                    return true;
                } else {
                    if (ConfigHandler.worldNotEnabled(worldName)) {
                        player.sendMessage(ChatColor.RED + "RTP in this world is not enabled!");
                        return true;
                    }
                    // If the player is already in the listed world or they have permissions to teleport there from anywhere
                    if (world.equals(player.getWorld()) || player.hasPermission("craftersnexus.rtp." + worldName)) {
                        // All checks passed, do random teleport!
                        return doRandomTeleport(player, world);
                    } else {
                        player.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
                        return true;
                    }
                }
            }
            // Sub commands follow
            else {
                // Too many arguments
                if (args.length > 2) {
                    commandSender.sendMessage(ChatColor.RED + "Too many arguments!");
                    return true;
                }
                // Remove world from config. Can be done from console
                if (args[0].equalsIgnoreCase("remove")) {
                    return new RTPSubCommand().rtpRemoveCommand(commandSender, args);
                }
                // Add world to config. Can be done from console
                if (args[0].equalsIgnoreCase("add")) {
                    return new RTPSubCommand().rtpAddCommand(commandSender, args);
                }
                else {
                    if (args.length == 2) {
                        // Executable by player or console. Handles cases where you want to teleport another player.
                        return new RTPSubCommand().rtpOthersCommand(commandSender, args);
                    } else {
                        commandSender.sendMessage(ChatColor.RED + "Unknown command: " + command + " " + Arrays.toString(args));
                        return false;
                    }
                }
            }
        }
        return false;
    }

    private boolean isAddOrRemoveCommand(String arg) {
        return arg.equalsIgnoreCase("add") || arg.equalsIgnoreCase("remove");
    }

    static boolean doRandomTeleport(Player player, World world) {
        ConsoleCommandSender console = Bukkit.getConsoleSender();
        String worldName = world.getName();
        boolean aroundPlayerMode = ConfigHandler.getAroundPlayerMode();
        double radius;
        if(aroundPlayerMode) {
            radius = ConfigHandler.getAroundPlayerRadius();
        } else {
            radius = ConfigHandler.getRadius();
        }
        console.sendMessage("Starting RTP for: " + player.getDisplayName() + " in world: " + worldName);
        player.sendMessage(ChatColor.GREEN + "Looking for safe location to teleport to...");

        // Do random teleport
        BukkitTask task = new RandomTeleporter(radius, world, player, aroundPlayerMode).runTaskTimerAsynchronously(CraftersNexusRTP.getPlugin(CraftersNexusRTP.class), 1L, 1L);
        return true;
    }
}
