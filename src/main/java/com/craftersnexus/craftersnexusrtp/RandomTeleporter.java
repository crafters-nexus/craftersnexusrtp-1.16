package com.craftersnexus.craftersnexusrtp;


import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.ThreadLocalRandom;

public class RandomTeleporter extends BukkitRunnable {
    private final WorldBorder border;
    private final World world;
    private final Player player;
    private final Location playerLoc;
    private final double minX;
    private final double maxX;
    private final double minZ;
    private final double maxZ;
    private int attempts = 0;
    private final int maxTries = ConfigHandler.getMaxTries();

    public RandomTeleporter(double radius, World world, Player player, boolean aroundPlayerMode) {
        this.world = world;
        this.border = world.getWorldBorder();
        this.player = player;
        this.playerLoc = this.player.getLocation();

        // Set min and max values for search
        if(aroundPlayerMode) {
            double playerX = player.getLocation().getX();
            double playerZ = player.getLocation().getZ();
            this.minX = playerX - radius;
            this.maxX = playerX + radius;
            this.minZ = playerZ - radius;
            this.maxZ = playerZ + radius;
        } else {
            double spawnX = world.getSpawnLocation().getX();
            double spawnZ = world.getSpawnLocation().getZ();
            this.minX = spawnX - radius;
            this.maxX = spawnX + radius;
            this.minZ = spawnZ - radius;
            this.maxZ = spawnZ + radius;
        }
    }

    private boolean isInsideBorder(Location loc) {
        return border.isInside(loc);
    }

    @Override
    public void run() {
        if(attempts > maxTries) {
            player.sendMessage(ChatColor.RED + "Unable to find a safe location to teleport to.");
            this.cancel();
        }
        ConsoleCommandSender console = Bukkit.getConsoleSender();
        double x = ThreadLocalRandom.current().nextInt((int) minX, (int) maxX);
        double z = ThreadLocalRandom.current().nextInt((int) minZ, (int) maxZ);
        double y = world.getHighestBlockYAt((int) x, (int) z) + 1;
        Location newLoc = new Location(world, x + 0.5, y, z + 0.5);
        Block newBlock = newLoc.getBlock();
        // Get Block under player
        Block underPlayer = newBlock.getRelative(0, -1, 0);
        // Get block above player
        Block topBlock = newBlock.getRelative(0, 1, 0);

        console.sendMessage("Random Location -> x: " + x + " y: " + y + " z: " + z);
        console.sendMessage("New block: " + newBlock.getType().name());
        console.sendMessage("Top block: " + topBlock.getType().name());
        console.sendMessage("Under Player: " + underPlayer.getType().name());

        if(isInsideBorder(newLoc)) {
            if (newBlock.isEmpty()) {
                if (topBlock.isEmpty()) {
                    if(!underPlayer.isLiquid()) {
                        if (!underPlayer.isEmpty()) {
                            if (!newLoc.equals(playerLoc)) {
                                if (!underPlayer.getType().equals(Material.BEDROCK)) {
                                    // Cancel async task and schedule TP task to run in a second
                                    this.cancel();
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            player.teleport(newLoc);
                                            player.sendMessage(ChatColor.GREEN + "You have been randomly teleported to " + x + ", " + y + ", " + z);
                                        }
                                    }.runTaskLater(CraftersNexusRTP.getPlugin(CraftersNexusRTP.class), 20);
                                }
                            }
                        }
                    }
                }
            }
        }
        attempts++;
    }
}
