package com.craftersnexus.craftersnexusrtp;

import com.craftersnexus.craftersnexusrtp.commands.RTPCommand;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;
import java.util.logging.Logger;


public class CraftersNexusRTP extends JavaPlugin {
    @Override
    public void onEnable() {
        Logger logger = this.getLogger();
        // Dump a configuration file
        saveDefaultConfig();
        logger.info("Registering commands...");
        // Register the command
        Objects.requireNonNull(this.getCommand("rtp")).setExecutor(new RTPCommand());
        logger.info("Registered RTP commands");
    }
}
