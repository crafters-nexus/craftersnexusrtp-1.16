package com.craftersnexus.craftersnexusrtp;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;

public class ConfigHandler {
    private static File configFile;
    private static FileConfiguration config;
    private static final File df = getPlugin().getDataFolder();

    // Loads the config file
    private static void load() {
        configFile = new File(df, "config.yml");
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    // Saves the config files
    private static void save() {
        try {
            config.save(configFile);
        } catch(Exception e) {
            Bukkit.getConsoleSender().sendMessage("Error saving config file: " + configFile.getName());
        }
    }

    // Get the plugin from the main class
    private static Plugin getPlugin() {
        return CraftersNexusRTP.getPlugin(CraftersNexusRTP.class);
    }

    // Get whether RTP is enabled in that world.
    public static boolean worldNotEnabled(String worldName) {
        load();
        return !config.getBoolean("worlds." + worldName + ".enabled");
    }

    // Remove a world from RTP
    public static void removeRtpWorld(String worldName) {
        load();
        // Remove the world from the config
        config.set("worlds." + worldName + ".enabled", null);
        // Save the file
        save();
    }

    // Add a world to RTP
    public static void addRtpWorld(String worldName) {
        load();
        // Remove the world from the config
        config.set("worlds." + worldName + ".enabled", true);
        // Save the file
        save();
    }

    public static double getRadius() {
        load();
        return config.getDouble("radius");
    }

    public static int getMaxTries() {
        load();
        return config.getInt("max-tries");
    }

    public static boolean getAroundPlayerMode() {
        load();
        return config.getBoolean("around-player-mode");
    }

    public static double getAroundPlayerRadius() {
        load();
        return config.getDouble("around-player-radius");
    }
}