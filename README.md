# A better spigot 1.13+ RTP plugin

# Setup
1. Go to a world, and run `/rtp add` as an operator or see the permissions section to add permissions
2. Optionally, configure a worldborder for the world. Use a square shape for best results
3. Add permissions for your default roles and for staff (see section below)
4. Make sure the plugin's config is setup to your liking for worlds without world borders and for max tries to find a safe location

## Commands and Permissions

- `/rtp` 
	- Randomly teleport in your current world
	- Permission: `craftersnexus.rtp`
	- Player only command
- `/rtp add [worldName]` 
	- Add your current world or the specified world to the list of active RTP dimensions
	- Permission: `craftersnexus.rtp.add`
- `/rtp remove [worldName]` 
	- Remove your current world or the specified world from the list of active RTP dimensions
	- Permission: `craftersnexus.rtp.remove`
- `/rtp <worldName>` 
	- Randomly teleport yourself to the specified world
	- Permission: `craftersnexus.rtp.<worldName>` (permission is ignored if in that dimension when using this command)
	- Player only command
- `/rtp <playerName> <worldName>`
	- Randomly teleport the specified player to the specified world
	- Permission: `craftersnexus.rtp.others`

# To develop

For IntellijI run: `gradle idea` and then business as usual:

- `gradle clean`
- `gradle build`
